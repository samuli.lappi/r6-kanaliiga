# R6 kanaliiga sääntöhahmotelma

## Versiohistoria

| Versio | Pvm | Kommentit |
| ------ | ------ | ------ |
| 0.0.1 | 2019-09-07 | Ensimmäinen draft |
| 0.0.2 | 2019-09-08 | CS-oppaasta kopioitu yleisiä asioita |

## Yleistä

[Kanaliigan yleiset säännöt](https://www.kanaliiga.fi/saannot/) ovat voimassa.
Nämä säännöt ovat lisäyksiä sekä pelikohtaisia sääntöjä.

Kapteenin tärkeimmät asiat muistaa ovat:

1. Seuraa Kanaliigan Discordia. Tiedottaminen pääsääntöisesti tapahtuu 
Discord-kanavamme kautta. Mikäli olet joukkueesi kapteeni, lue kaikki 
tiedotukset #r6_turnausinfo –kanavalta sekä #r6-kapteenit –kanavalta.

2. Sovi jokaisen viikon pelit mahdollisimman ajoissa ja merkitse ne 
Google-kalenteriin. Ohjeet kalenterin käytöstä löytyy tästä oppaassa.

3. Kanaliiga on vapaaehtoisvoimin toimiva yhteisö. Teemme parhaamme, jotta 
turnauksemme ja muu toiminta on yhteisömme jäsenille mahdollisimman mukavaa. Jos 
sinulle tulee kehitysehdotuksia, otamme niitä ilomielin vastaan kanavalla tai 
yksityisviestillä suoraan R6 ylläpidolle.

4. Noudata sääntöjä, pelaa rehdisti ja ennen kaikkea: pidä hauskaa! Kanaliiga on 
amatööriliiga, jossa pelataan toki tosissaan, mutta tärkeintä on kaikkien 
viihtyminen. 

5. Jos mietityttää, kysy! Ylläpito auttaa mielellään Discordissa niin 
kanavillamme kuin yksityisviesteillä.

### Ylläpidon yhteystiedot

Kanaliigan Discordin kutsulinkki: 
[https://discord.gg/e44ZjXP](https://discord.gg/e44ZjXP)


## Ilmoittautuminen

Ilmoittautuminen tapahtuu toornament.com sivustolla. Kapteenin ilmoittaessa 
joukkueensa turnaukseen tulee ilmoituksessa olla joukkueen nimi, edustettava 
yritys, kapteenin nimi sekä Uplay- ja Discord-tili. Lisäksi Kapteeni ilmoittaa 
vähintään neljä (4) muuta pelaajaa ja enintään kolme (3) varapelaajaa sekä 
heidän tietonsa. Uplay-tili tulee linkittää suoraan pelaajan profiiliin. 
Ilmoitettu tili on oltava pelaajan päätili, vaihtoehtoisia (smurf) tilejä ei 
sallita.

Kaikkien kapteenien tulee olla tavoitettavissa Kanaliigan Discordissa. Ylläpito 
lisää joukkueen kapteenin #r6-kapteenit -kanavalle, jossa tiedotamme tarkemmin 
turnauksen etenemiseen liittyvistä asioista.

### Varapelaajat

Turnaukseen voi ilmoittaa yhteensä kahdeksan (8) pelaajaa. Joukkue saa valita 
ilmoitetuista pelaajista viisi pelaajaa jokaiseen pelaamaansa peliin. Joukkueen 
tulee kuitenkin pelata peli loppuun niillä viidellä pelaajalla, jotka ovat 
pelipalvelimella pelin alkaessa. Paras kolmesta-sarjoissa pelaajia ei saa 
vaihtaa karttojen välissä olevalla teknisellä tauolla.

Erillisestä pyynnöstä ylläpito voi hyväksyä lisäpelaajan turnauksen jo ollessa 
käynnissä. Nämä ovat kuitenkin poikkeustilanteita, jotka ylläpito ratkaisee aina 
tapauskohtaisesti.

Mikäli samasta yrityksestä on ilmoittautunut useampi tiimi, voi tiimi käyttää 
varapelaajaa yrityksen toisesta joukkueesta, mikäli tämän pelaajan taitotaso ei 
nosta tiimin keskiarvoista taitotasoa. Käytettäessä varapelaajaa yrityksen 
toisesta joukkueesta, tulee aina ylläpidolta varmistaa halutun varapelaajan 
taitotason vaikuttaminen joukkueen keskiarvoon.

### Huijausepäilyt

Ylläpito tekee kaikkensa, jotta pelit Kanaliigassa olisivat reiluja kaikkia 
pelaajia kohtaan. Pelejä on kuitenkin kymmeniä viikossa, joten ylläpito ei pysty 
valvomaan jokaista ottelua. Paras valvonta turnauksessa on vertaisvalvonta ja 
ylläpito toivoo jokaisen pelaajan pitävän silmänsä auki. Mikäli pelaaja epäilee 
huijaamista oman tai vastustajajoukkueen osalta, tulee tästä ilmoittaa 
ylläpidolle mahdollisimman pian todistusaineistojen kanssa. Ylläpito tutkii 
jokaisen ilmiannon Kanaliigan muiden ylläpitäjien kanssa ja ilmoittaa, mikäli 
perusteita jatkotoimenpiteille ilmenee.

Kanaliiga toivoo, että ketään ei syytetä huijaamisesta (esim. smurffaaminen tai 
huijauskoodaus) ilman perusteita. Tutkintatapauksissa jokainen turnauksessa 
mukana oleva pelaaja sitoutuu auttamaan tutkinnan etenemistä vastaamalla 
rehellisesti kysymyksiin, tarjoamalla kaikki pyydetyt aineistot sekä hyväksymään 
mahdollisen ylläpidon kaveripyynnön Uplayssä.

Ylläpito voi vaatia yksittäisiä pelaajia tai joukkueita käyttämään MOSS 
anticheat järjestelmää jos huijausepäilyjä esiintyy.

### Rangaistukset

Ylläpito pidättää oikeuden päättää rangaistuksista tapauskohtaisesti.

# Turnauksen kulku

Ennen turnausta julkaistaan turnausaikataulu Toornamentissa sekä Discordissa. 
Aikataulussa jokaiselle joukkueelle on ilmoitettu pelattavat ottelut 
päivätasolla. Otteluita on viikossa yksi, pelit pelataan tiistaisin kello 20. 
Joukkueet voivat yhdessä sopia ottelun siirtämisestä toiseen ajankohtaan, mutta 
kaikkien viikon otteluiden tulee olla pelattuna viikon loppuun mennessä.
Turnausmuodosta johtuen otteluita ei voida siirtää seuraaville viikoille.

Turnaus alkaa viikolla 42. Aikataulu:

- 15.10: WB Round 1
- 22.10: WB Round 2 & LB Round 1
- 29.10: WB Round 3 & LB Round 2
- 5.11: LB Round 3
- 12.11:  WB Round 4 & LB Round 4
- 19.11: LB Round 5
- 26.11: LB Round 6
- 3.12: Grand Final

## Termistö

- Kotijoukkue = Toornamentin My Matches sivulla ylempi joukkue
- Vierasjoukkue = Toornamentin My Matches sivulla alempi joukkue

## Asetukset

Otteluissa käytetään seuraavia asetuksia

| Asetus | Arvo |
| ------ | ------ |
| Playlist Type | Normal Mode |
| Server Type | Dedicated Server | 
| Voice Chat | Team Only |
| Time of the Day | Day |
| HUD Settings | Pro League |

Match Settings:

| Asetus | Arvo |
| ------ | ------ |
| Number of Bans | 4 |
| Ban Timer | 15 |
| Number of Rounds | 12 |
| Attacker/Defender Role swap | 6 |
| Overtime Rounds | 3 |
| Overtime Score Difference | 1 |
| Overtime Role Change | 1 |
| Objective Rotation Parameter | 2 |
| Objective Type Rotation | Rounds Played |
| Attacker Unique Spawn | On |
| Pick Phase Timer | 30 |
| 6TH Pick Phase | On |
| 6TH Pick Phase Timer | 30 |
| Reveal Phase Timer | 9 |
| Damage Handicap | 100 |
| Friendly Fire Damage | 100 |
| Injured | 20 |
| Sprint | On |
| Lean | On |
| Death Replay | Off |

Game Mode Settings

| Asetus | Arvo |
| ------ | ------ |
| Game Mode | Bomb |
| Plant Duration | 7 |
| Defuse Duration | 7 |
| Fuse Time | 45 |
| Defuse Carrier Selection | On |
| Preparation Phase Duration | 45 |
| Action Phase Duration | 180 |

Suositeltavaa on, että jokaisen joukkueen kapteeni tekee valmiiksi playlistin
näillä asetuksilla, ja vaihtaa ottelukohtaisesti kartan oikeaksi.

## Kartat

Kartat ovat ESL map poolin kartat, jotka ovat tällä hetkellä:

- Bank
- Border
- Kafe Dostoyevsky
- Club House
- Consulate
- Coastline
- Villa 

## Map veto

Otteluissa pelataan yksi kartta. Kartan valinnassa käytetään 
[mapban.eu](https://www.mapban.eu/en/ban/r6s) sivustoa. Kotijoukkueen kapteeni 
luo bannaus lobbyn ja antaa linkin vastustajajoukkueen kapteenille. Kotijoukkue 
aloittaa bannauksen. Järjestys on:

- Kotijokkue ban
- Vierasjoukkue ban
- Kotijoukkue ban
- Vierasjoukkue ban
- Kotijoukkue ban
- Vierasjoukkue ban

Jäljelle jäänyt kartta pelataan. Kotijoukkue valitsee aloituspuolen. 
Vierasjoukkue valitsee jatkoajan aloituspuolen, ja ilmoittaa sen kotijoukkueen
kapteenille esimerkiksi Discordissa (mapban.eu ei tue jatkoajan puolenvalintaa).

## Pelin aloitus

Kotijoukkueen kapteeni luo Custom gamen, jossa on yllä mainitut asetukset.
Kapteeni kutsuu sitten peliin oman joukkueensa, sekä vierasjoukkueen kapteenin.
Vierasjoukkueen kapteeni kutsuu sitten pelaajansa mukaan.

Kotijoukkueen kapteeni laittaa pelin asetuksiin oikeat jatkoajan puolet sekä
joukkueiden nimet.

Peli voidaan aloittaa kun kaikki lobbyssä olevat pelaajat ovat kuitanneet
tilakseen "Ready".

## Rehostaus

Kierrosta, joka on jo alkanut, ei keskeytetä teknisten ongelmien takia. Jos
pelaaja tippuu pelistä, pyritään ensisijaisesti siihen että hän ehtii takaisin
ennen seuraavan kierroksen alkua. Jos näin ei käy, peli rehostataan. Kotijoukkue
laittaa round historyn sekä operaattori bannit kuntoon ennen pelin aloitusta.

Kumpikin joukkue voi pyytää rehostausta korkeintaan kerran ottelun aikana.

## Operaattorit

Seasonin uudet operaattorit ovat kiellettyjä. Tällä kaudella ne ovat Amaru ja 
Goyo.

## Ottelun jälkeen

Kun ottelu on pelattu, voittaneen joukkueen kapteenin vastuulla on viedä ottelun 
tulos Toornamentiin. Tuloksen yhteyteen lisätään kuvakaappaus scoreboardista.